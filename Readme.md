# Technical Vanilla

This mod, made for Modjam, adds Sponge Worldgen (different ID then Vanilla one). Using this Sponges, you can Power Vanilla Blocks to give them additional Functions. A detailed List is provided below.

## How to begin

To use this Mod, you first will need Silk-Touch Sheers and harvest Sponges, spawning on the ground off Oceans. If you can make Redstone and LapisLazuli Sponges by Right clicking on them with the depending Item. If you move a Redstone Sponge (using a Piston) next to a LapisLazuli Sponge, there will be creating Energy inside the Lapis Sponge. (It is good if you use a redstone clock to always push and pull the redstone Sponge next to the Lapis Sponge to create enough energy). And now: If you place an Vanilla Block next to the Lapis Sponge it will get powered.

## Supported Vanilla Blocks

### Anvil
Lets u combine Enchantments up to one level higher and have no "to expensive"...

### Hopper
Transports a hole ItemStack

### Netherrack
Only powerable in Nether - will turn all Nether Blocks in a 32x32x32 cube around the powered block to the depending overworld ones (even randomises the Stone-Brick Metadata)

### Bedrock
Becomes breakable (beside at level 0 and 128 in the Nether)

### Cactus
Doesn't grow

### Cake Block
try it out yourself

### Sand
Doesn't fall and powers Cactus on top off it.

### Gravel
Doesn't fall

### Brewing Stand
Using Poisoned Lily Pad you can make a tear III and an extended+ potion (depending on the Potion before brewing)

### Lily Pad
Slowly becomes Poisoned

### Redstone Repeater
have an way longer delay

### Redstone Torch
can't burn out

### Piston
can push up to (I think) 25 Block.

### Sticky Piston
as Piston plus can pull up to (I think) 15 Blocks if the last Block to pull is an Block of Iron.

## Trapped Chest
deals 10 (5 Herts) of damage when opening

## Iron Bars
deals 3 (1.5 Herts) of damage while touching or standing on.+- 

## And now to the really fancy part:
### Dragon Egg
Spawns an "Charged Ender Dragon", which is at the moment exactly as strong as the normal Ender Dragon, but drops 2 Dragon Eggs and can't destroy Blocks (for the fancy Enderman-Farms) (The Eggs gets destroyed when the Dragon is spawned)

## Download:
See "Downloads"

## Installation:
- Install Forge
- Drag and Drop the mod in the mods folder in your .minecraft folder.

## Compile from Source:
- Decompile Minecraft using MCP and Forge
- Drag and Drop all files from the Source in your mcp directory
- Run Recompile
- Run Start Client

## Make sure you are using Version 1.0.0 (Tag) if you want to use the Modjam version
(modding going on after Modjam)

## Timelaps
The timelaps for this Modjam mod can be found at [Youtube](http://www.youtube.com/watch?v=PHK8lbQfINs)