package ccfan.technicalVanilla.proxies;

import ccfan.technicalVanilla.entity.EntityDragon;
import ccfan.technicalVanilla.renderers.DragonEggRenderer;
import ccfan.technicalVanilla.renderers.RenderDragon;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {
	public static int DragonEggRenderType;    

	
	
	
	@Override
	public void setCustomRenderers()
    {
		
		DragonEggRenderType = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(new DragonEggRenderer());
        
        RenderingRegistry.registerEntityRenderingHandler(EntityDragon.class, new RenderDragon());
    }
}
