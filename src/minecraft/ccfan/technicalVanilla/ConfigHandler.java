package ccfan.technicalVanilla;

import java.io.File;

import ccfan.technicalVanilla.blocks.IBlockElectrifiable;
import net.minecraftforge.common.Configuration;

public class ConfigHandler {
	public static void init(File file)  {
		Configuration config = new Configuration(file);
		
		config.load();
		ModInformation.BLOCK_SPONGE_ID = config.get("Blocks", "Block_Sponge", ModInformation.BLOCK_SPONGE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_REDSTONE_SPONGE_ID = config.get("Blocks", "Block_RedstoneSponge", ModInformation.BLOCK_REDSTONE_SPONGE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_LAPIS_SPONGE_ID = config.get("Blocks", "Block_LapisLazuliSponge", ModInformation.BLOCK_LAPIS_SPONGE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_POISON_LILY_PAD_ID = config.get("Blocks", "Block_PoisonLilyPad", ModInformation.BLOCK_POISON_LILY_PAD_ID_DEFAULT).getInt();
		
		ModInformation.BLOCK_BEDROCK_ID = config.get("ElectrifiableBlocks", "Block_EBedrock", ModInformation.BLOCK_BEDROCK_ID_DEFAULT).getInt();
		ModInformation.BLOCK_CAKE_ID = config.get("ElectrifiableBlocks", "Block_ECake", ModInformation.BLOCK_CAKE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_CACTUS_ID = config.get("ElectrifiableBlocks", "Block_ECactus", ModInformation.BLOCK_CACTUS_ID_DEFAULT).getInt();
		ModInformation.BLOCK_SAND_ID = config.get("ElectrifiableBlocks", "Block_ESand", ModInformation.BLOCK_SAND_ID_DEFAULT).getInt();
		ModInformation.BLOCK_GRAVEL_ID = config.get("ElectrifiableBlocks", "Block_EGravel", ModInformation.BLOCK_GRAVEL_ID_DEFAULT).getInt();
		ModInformation.BLOCK_NETHERRACK_ID = config.get("ElectrifiableBlocks", "Block_ENetherrack", ModInformation.BLOCK_NETHERRACK_ID_DEFAULT).getInt();
		ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_ID = config.get("ElectrifiableBlocks", "Block_ERedstoneTorchActive", ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_REDSTONE_TORCH_IDLE_ID = config.get("ElectrifiableBlocks", "Block_ERedstoneTorchInactive", ModInformation.BLOCK_REDSTONE_TORCH_IDLE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_REDSTONE_REPEATER_ACTIVE_ID = config.get("ElectrifiableBlocks", "Block_ERedstoneRepeaterActive", ModInformation.BLOCK_REDSTONE_REPEATER_ACTIVE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_REDSTONE_REPEATER_IDLE_ID = config.get("ElectrifiableBlocks", "Block_ERedstoneRepeaterInactive", ModInformation.BLOCK_REDSTONE_REPEATER_IDLE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_PISTON_BASE_ID = config.get("ElectrifiableBlocks", "Block_EPistonBase", ModInformation.BLOCK_PISTON_BASE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_PISTON_STICKY_BASE_ID = config.get("ElectrifiableBlocks", "Block_ESitckyPistonBase", ModInformation.BLOCK_PISTON_STICKY_BASE_ID_DEFAULT).getInt();
		ModInformation.BLOCK_PISTON_EXTENSION_ID = config.get("ElectrifiableBlocks", "Block_EPistonExtension", ModInformation.BLOCK_PISTON_EXTENSION_ID_DEFAULT).getInt();
		ModInformation.BLOCK_ANVIL_ID = config.get("ElectrifiableBlocks", "Block_EAnvil", ModInformation.BLOCK_ANVIL_ID_DEFAULT).getInt();
		ModInformation.BLOCK_BREWING_STAND_ID = config.get("ElectrifiableBlocks", "Block_EBrewingStand", ModInformation.BLOCK_BREWING_STAND_ID_DEFAULT).getInt();
		ModInformation.BLOCK_LILY_PAD_ID = config.get("ElectrifiableBlocks", "Block_ELilyPad", ModInformation.BLOCK_LILY_PAD_ID_DEFAULT).getInt();
		ModInformation.BLOCK_DRAGON_EGG_ID = config.get("ElectrifiableBlocks", "Block_EDragonEgg", ModInformation.BLOCK_DRAGON_EGG_ID_DEFAULT).getInt();
		ModInformation.BLOCK_HOPPER_ID = config.get("ElectrifiableBlocks", "Block_EHopper", ModInformation.BLOCK_HOPPER_ID_DEFAULT).getInt();
		ModInformation.BLOCK_TRAPPED_CHEST_ID = config.get("ElectrifiableBlocks", "Block_ETrappedChest", ModInformation.BLOCK_TRAPPED_CHEST_ID_DEFAULT).getInt();
		ModInformation.BLOCK_IRON_BARS_ID = config.get("ElectrifiableBlocks", "Block_EIronBars", ModInformation.BLOCK_IRON_BARS_ID_DEFAULT).getInt();
		
		config.save();
	}
}
