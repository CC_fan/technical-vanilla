package ccfan.technicalVanilla;

public class ModInformation {
	public static final String ID = "technicalVanilla";
	public static final String NAME = "Technical Vanilla";
	public static final String VERSION = "0.0.1.0";
	public static final String CHANNEL = "techVanilla";
	
	//----------------------------------------------------
	public static final String TEXTURE_LOCATION = "technicalvanilla";
	
	public static final int BLOCK_SPONGE_ID_DEFAULT = 2100;
	public static int BLOCK_SPONGE_ID;
	public static final String BLOCK_SPONGE_KEY = "Sponge";
	public static final String BLOCK_SPONGE_UNLOCALIZED_NAME = "sponge_T";
	public static final String BLOCK_SPONGE_NAME = "Sponge";

	public static final int BLOCK_REDSTONE_SPONGE_ID_DEFAULT = 2101;
	public static int BLOCK_REDSTONE_SPONGE_ID;
	public static final String BLOCK_REDSTONE_SPONGE_KEY = "RedstoneSponge";
	public static final String BLOCK_REDSTONE_SPONGE_UNLOCALIZED_NAME = "redstoneSponge";
	public static final String BLOCK_REDSTONE_SPONGE_NAME = "Redstone Sponge";
	public static final String BLOCK_REDSTONE_SPONGE_ICON = "RedstoneSponge";
	
	public static final int BLOCK_LAPIS_SPONGE_ID_DEFAULT = 2102;
	public static int BLOCK_LAPIS_SPONGE_ID;
	public static final String BLOCK_LAPIS_SPONGE_KEY = "LapislazuliSponge";
	public static final String BLOCK_LAPIS_SPONGE_UNLOCALIZED_NAME = "LapislazuliSponge";
	public static final String BLOCK_LAPIS_SPONGE_NAME = "LapisLazuli Sponge";
	public static final String BLOCK_LAPIS_SPONGE_ICON = "LapislazuliSponge";
	
	public static final int BLOCK_BEDROCK_ID_DEFAULT = 2103;
	public static int BLOCK_BEDROCK_ID;
	public static final String BLOCK_BEDROCK_KEY = "Bedrock_T";
	public static final String BLOCK_BEDROCK_UNLOCALIZED_NAME = "bedrock_t";
	public static final String BLOCK_BEDROCK_NAME = "Berdock";

	public static final int BLOCK_CAKE_ID_DEFAULT = 2104;
	public static int BLOCK_CAKE_ID;
	public static final String BLOCK_CAKE_KEY = "Cake_T";
	public static final String BLOCK_CAKE_UNLOCALIZED_NAME = "cake_t";
	public static final String BLOCK_CAKE_NAME = "Cake";
	
	public static final int BLOCK_CACTUS_ID_DEFAULT = 2105;
	public static int BLOCK_CACTUS_ID;
	public static final String BLOCK_CACTUS_KEY = "Cactus_T";
	public static final String BLOCK_CACTUS_UNLOCALIZED_NAME = "cactus_t";
	public static final String BLOCK_CACTUS_NAME = "Cactus";

	public static final int BLOCK_SAND_ID_DEFAULT = 2106;
	public static int BLOCK_SAND_ID;
	public static final String BLOCK_SAND_KEY = "Sand_T";
	public static final String BLOCK_SAND_UNLOCALIZED_NAME = "sand_t";
	public static final String BLOCK_SAND_NAME = "Sand";

	public static final int BLOCK_GRAVEL_ID_DEFAULT = 2107;
	public static int BLOCK_GRAVEL_ID;
	public static final String BLOCK_GRAVEL_KEY = "Gravel_T";
	public static final String BLOCK_GRAVEL_UNLOCALIZED_NAME = "gravel_t";
	public static final String BLOCK_GRAVEL_NAME = "Gravel";

	public static final int BLOCK_NETHERRACK_ID_DEFAULT = 2108;
	public static int BLOCK_NETHERRACK_ID;
	public static final String BLOCK_NETHERRACK_KEY = "Netherrack_T";
	public static final String BLOCK_NETHERRACK_UNLOCALIZED_NAME = "netherrack_t";
	public static final String BLOCK_NETHERRACK_NAME = "Netherrack";

	public static final int BLOCK_REDSTONE_TORCH_ACTIVE_ID_DEFAULT = 2109;
	public static int BLOCK_REDSTONE_TORCH_ACTIVE_ID;
	public static final String BLOCK_REDSTONE_TORCH_ACTIVE_KEY = "notGate_T";
	public static final String BLOCK_REDSTONE_TORCH_ACTIVE_UNLOCALIZED_NAME = "notGate_t";
	public static final String BLOCK_REDSTONE_TORCH_ACTIVE_NAME = "Redstone Torch";

	public static final int BLOCK_REDSTONE_TORCH_IDLE_ID_DEFAULT = 2110;
	public static int BLOCK_REDSTONE_TORCH_IDLE_ID;
	public static final String BLOCK_REDSTONE_TORCH_IDLE_KEY = "notGateI_T";
	public static final String BLOCK_REDSTONE_TORCH_IDLE_UNLOCALIZED_NAME = "notGate_t";
	public static final String BLOCK_REDSTONE_TORCH_IDLE_NAME = "Redstone Torch";

	public static final int BLOCK_REDSTONE_REPEATER_ACTIVE_ID_DEFAULT = 2111;
	public static int BLOCK_REDSTONE_REPEATER_ACTIVE_ID;
	public static final String BLOCK_REDSTONE_REPEATER_ACTIVE_KEY = "diode_T";
	public static final String BLOCK_REDSTONE_REPEATER_ACTIVE_UNLOCALIZED_NAME = "diode_t";
	public static final String BLOCK_REDSTONE_REPEATER_ACTIVE_NAME = "Redstone Repeater";

	public static final int BLOCK_REDSTONE_REPEATER_IDLE_ID_DEFAULT = 2112;
	public static int BLOCK_REDSTONE_REPEATER_IDLE_ID;
	public static final String BLOCK_REDSTONE_REPEATER_IDLE_KEY = "diodeI_T";
	public static final String BLOCK_REDSTONE_REPEATER_IDLE_UNLOCALIZED_NAME = "diode_t";
	public static final String BLOCK_REDSTONE_REPEATER_IDLE_NAME = "Redstone Repeater";

	public static final int BLOCK_PISTON_BASE_ID_DEFAULT = 2113;
	public static int BLOCK_PISTON_BASE_ID;
	public static final String BLOCK_PISTON_BASE_KEY = "pistonBase_T";
	public static final String BLOCK_PISTON_BASE_UNLOCALIZED_NAME = "Piston_t";
	public static final String BLOCK_PISTON_BASE_NAME = "Piston";

	public static final int BLOCK_PISTON_STICKY_BASE_ID_DEFAULT = 2114;
	public static int BLOCK_PISTON_STICKY_BASE_ID;
	public static final String BLOCK_PISTON_STICKY_BASE_KEY = "pistonStickyBase_T";
	public static final String BLOCK_PISTON_STICKY_BASE_UNLOCALIZED_NAME = "StickyPiston_t";
	public static final String BLOCK_PISTON_STICKY_BASE_NAME = "Sticky Piston";
	
	public static final int BLOCK_PISTON_EXTENSION_ID_DEFAULT = 2115;
	public static int BLOCK_PISTON_EXTENSION_ID;
	public static final String BLOCK_PISTON_EXTENSION_KEY = "pistonExtension_T";
	public static final String BLOCK_PISTON_EXTENSION_UNLOCALIZED_NAME = "PistonExtension_t";

	public static final int BLOCK_ANVIL_ID_DEFAULT = 2116;
	public static int BLOCK_ANVIL_ID;
	public static final String BLOCK_ANVIL_KEY = "anvil_T";
	public static final String BLOCK_ANVIL_UNLOCALIZED_NAME = "Anvil_t";
	public static final String BLOCK_ANVIL_NAME = "Anvil";

	public static final int BLOCK_BREWING_STAND_ID_DEFAULT = 2117;
	public static int BLOCK_BREWING_STAND_ID;
	public static final String BLOCK_BREWING_STAND_KEY = "brewingStand_T";
	public static final String BLOCK_BREWING_STAND_UNLOCALIZED_NAME = "BrewingStand_t";
	public static final String BLOCK_BREWING_STAND_NAME = "Brewing Stand";

	public static final int BLOCK_LILY_PAD_ID_DEFAULT = 2118;
	public static int BLOCK_LILY_PAD_ID;
	public static final String BLOCK_LILY_PAD_KEY = "lilyPad_T";
	public static final String BLOCK_LILY_PAD_UNLOCALIZED_NAME = "LilyPad_t";
	public static final String BLOCK_LILY_PAD_NAME = "Lily Pad";

	public static final int BLOCK_POISON_LILY_PAD_ID_DEFAULT = 2119;
	public static int BLOCK_POISON_LILY_PAD_ID;
	public static final String BLOCK_POISON_LILY_PAD_KEY = "poisonLilyPad_T";
	public static final String BLOCK_POISON_LILY_PAD_UNLOCALIZED_NAME = "PoisonLilyPad_t";
	public static final String BLOCK_POISON_LILY_PAD_NAME = "Poison Lily Pad";
	
	public static final int BLOCK_DRAGON_EGG_ID_DEFAULT = 2120;
	public static int BLOCK_DRAGON_EGG_ID;
	public static final String BLOCK_DRAGON_EGG_KEY = "dragonEgg_T";
	public static final String BLOCK_DRAGON_EGG_UNLOCALIZED_NAME = "DragonEgg_t";
	public static final String BLOCK_DRAGON_EGG_NAME = "Dragon Egg";

	public static final int BLOCK_HOPPER_ID_DEFAULT = 2121;
	public static int BLOCK_HOPPER_ID;
	public static final String BLOCK_HOPPER_KEY = "hopper_T";
	public static final String BLOCK_HOPPER_UNLOCALIZED_NAME = "Hopper_t";
	public static final String BLOCK_HOPPER_NAME = "Hopper";

	public static final int BLOCK_TRAPPED_CHEST_ID_DEFAULT = 2122;
	public static int BLOCK_TRAPPED_CHEST_ID;
	public static final String BLOCK_TRAPPED_CHEST_KEY = "tappedChest_T";
	public static final String BLOCK_TRAPPED_CHEST_UNLOCALIZED_NAME = "TrappedChest_t";
	public static final String BLOCK_TRAPPED_CHEST_NAME = "Trapped Chest";
	
	public static final int BLOCK_IRON_BARS_ID_DEFAULT = 2123;
	public static int BLOCK_IRON_BARS_ID;
	public static final String BLOCK_IRON_BARS_KEY = "ironBars_T";
	public static final String BLOCK_IRON_BARS_UNLOCALIZED_NAME = "IronBars_t";
	public static final String BLOCK_IRON_BARS_NAME = "Iron Bars";

	public static final String ITEM_POISON_LILY_PAD_KEY = "poisonLilyPadItem_T";
	public static final String ITEM_POISON_LILY_PAD_UNLOCALIZED_NAME = "PoisonLilyPadItem_t";
	
	
	public static final String ENTITY_ENDERDRAGON_KEY = "enderDragon_T";
	public static final String ENTITY_ENDERDRAGON_NAME = "Charged Ender Dragon";
	
	public static final String DEATH_TRAPPEDCHEST = "was electrolyed by";
	
	
}
