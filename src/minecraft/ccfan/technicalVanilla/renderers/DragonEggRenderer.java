package ccfan.technicalVanilla.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import ccfan.technicalVanilla.proxies.ClientProxy;
import ccfan.technicalVanilla.tileEntitys.TileEntityDragonEgg;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class DragonEggRenderer implements ISimpleBlockRenderingHandler {

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		// TODO Auto-generated method stub

	}

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
                    Block block, int modelId, RenderBlocks renderer) {
    	
        boolean flag = false;
        int l = 0;

        for (int i1 = 0; i1 < 8; ++i1)
        {
            byte b0 = 0;
            byte b1 = 1;

            if (i1 == 0)
            {
                b0 = 2;
            }

            if (i1 == 1)
            {
                b0 = 3;
            }

            if (i1 == 2)
            {
                b0 = 4;
            }

            if (i1 == 3)
            {
                b0 = 5;
                b1 = 2;
            }

            if (i1 == 4)
            {
                b0 = 6;
                b1 = 3;
            }

            if (i1 == 5)
            {
                b0 = 7;
                b1 = 5;
            }

            if (i1 == 6)
            {
                b0 = 6;
                b1 = 2;
            }

            if (i1 == 7)
            {
                b0 = 3;
            }

            float f = (float)b0 / 16.0F;
            float f1 = 1.0F - (float)l / 16.0F;
            float f2 = 1.0F - (float)(l + b1) / 16.0F;
            l += b1;
            
            
            TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
            float mul = 1;
            if (tileEntity instanceof TileEntityDragonEgg){
            	TileEntityDragonEgg tileEntityDragonEgg = (TileEntityDragonEgg) tileEntity;
            	mul = 1 + tileEntityDragonEgg.getProcess() * 0.8F;
            	System.out.println(tileEntityDragonEgg.getProcess());
            }
            
            renderer.setRenderBounds((double)(0.5F - f*mul), (double)f2 * mul, (double)(0.5F - f * mul), (double)(0.5F + f * mul), (double)f1 * mul, (double)(0.5F + f *mul));
            renderer.renderStandardBlock(block, x, y, z);
        }

        flag = true;
        renderer.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
        return flag;            
    }


	@Override
	public boolean shouldRender3DInInventory() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getRenderId() {
		// TODO Auto-generated method stub
		return ClientProxy.DragonEggRenderType;
	}

}
