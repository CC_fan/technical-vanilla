package ccfan.technicalVanilla.damageSource;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.DamageSource;
import net.minecraft.util.StatCollector;
import ccfan.technicalVanilla.blocks.Blocks;

public class DamageSourceElectic extends net.minecraft.util.DamageSource {

    public static DamageSource trappedChest = new DamageSourceElectic(Blocks.blockTrappedChest);	
    public static DamageSource ironBars = new DamageSourceElectic(Blocks.blockIronBars);	
	
    private Block block;
    
	protected DamageSourceElectic(Block block) {
		super("Electric");
		this.block = block;
	}
	
	
    public ChatMessageComponent getDeathMessage(EntityLivingBase par1EntityLivingBase)
    {
        String s1 = par1EntityLivingBase.getTranslatedEntityName() + " " + StatCollector.translateToLocal("death.attack." + this.damageType) + " " + block.getLocalizedName();
        return ChatMessageComponent.func_111066_d(s1);
    }

}

