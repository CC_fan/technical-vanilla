package ccfan.technicalVanilla.tileEntitys;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ElectrifiableBlockRegistry.BlockState;
import ccfan.technicalVanilla.blocks.IBlockElectrifiable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityLapisLazuliSponge extends TileEntity {
	private int EnergyStored = 0;
	
	public void addEnergy(int Energy){EnergyStored = Math.min(EnergyStored + (Energy > 0 ? Energy : 0), 60000) ;}
	public void useEnergy(int Energy){EnergyStored = Math.max(EnergyStored - (Energy > 0 ? Energy : 0), 0) ;}
	
	public int getEnergy() { return EnergyStored; }
	
	
	@Override
	public void writeToNBT(NBTTagCompound par1)
	{
		super.writeToNBT(par1);
		par1.setInteger("EnergyStored", EnergyStored);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound par1)
	{
		super.readFromNBT(par1);
		EnergyStored = par1.getInteger("EnergyStored");
	}
	
	@Override
	public void updateEntity() {
		if (!worldObj.isRemote){
			for (int i=1; i<=6; i++){
				updateBlocks(i);
			}
		}
	}
	
	private void updateBlocks(int side) {
		int x=xCoord; int y=yCoord; int z=zCoord;
		switch (side){
		case 1: x++; break;
		case 2: z++; break;
		case 3: x--; break;
		case 4: z--; break;
		case 5: y++; break;
		case 6: y--; break;
		default: break;
		}
		
		
		int meta = worldObj.getBlockMetadata(x, y, z);
		BlockState state = ElectrifiableBlockRegistry.getBlockState(worldObj, x, y, z);
		if (state!=BlockState.UNELECTRIFIABLE){
			IBlockElectrifiable block = ElectrifiableBlockRegistry.getBlock(worldObj, x, y, z);
			
			if (block.getEnergyCost() <= EnergyStored){
				if (state == BlockState.ELECTRIFIABLE){
					worldObj.setBlock(x, y, z, block.getElectrifiableID(), meta, block.shouldBlockUpdate(worldObj, x, y, z) ? 3: 2);
					block.onElectrifising(worldObj, x, y, z);
				}
				EnergyStored -= block.getEnergyCost();
			} else {
				if (state == BlockState.ELECTRIFIZED){
					worldObj.setBlock(x, y, z, block.getVanillaID(), meta, block.shouldBlockUpdate(worldObj, x, y, z) ? 3: 2);
					block.onUnelectrifising(worldObj, x, y, z);
				}
			}
		}
	}
	
	
	
}
