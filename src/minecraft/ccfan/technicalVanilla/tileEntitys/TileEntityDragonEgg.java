package ccfan.technicalVanilla.tileEntitys;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldProviderEnd;
import ccfan.technicalVanilla.entity.EntityDragon;

public class TileEntityDragonEgg extends TileEntity {

	private int SpawnCounter = 200;
	
	@Override
	public void writeToNBT(NBTTagCompound par1)
	{
		super.writeToNBT(par1);
		par1.setInteger("SpawnCounter", SpawnCounter);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound par1)
	{
		super.readFromNBT(par1);
		SpawnCounter = par1.getInteger("SpawnCounter");
	}


	public float getProcess(){
		return (200F-SpawnCounter)/200F;
	}
	
	
	@Override
	public void updateEntity() {
		System.out.println(SpawnCounter);
		if (SpawnCounter == 0) {
			if (this.getWorldObj().provider instanceof WorldProviderEnd){
				if (!this.getWorldObj().isRemote){
					Entity dragon = new EntityDragon(this.getWorldObj());//, xCoord, yCoord, zCoord);
		            dragon.setLocationAndAngles(xCoord, yCoord, zCoord, new Random().nextFloat() * 360.0F, 0.0F);
					this.getWorldObj().spawnEntityInWorld(dragon);
					worldObj.setBlockToAir(xCoord, yCoord, zCoord);

				}
			}
			SpawnCounter = 200;
		} else {
			SpawnCounter--;
		}
	}
	
	
	
}
