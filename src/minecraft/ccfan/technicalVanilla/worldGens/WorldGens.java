package ccfan.technicalVanilla.worldGens;

import cpw.mods.fml.common.registry.GameRegistry;

public class WorldGens {

	public static WorldGeneraterSponge worldGenSponge;
	
	public static void init(){
		worldGenSponge = new WorldGeneraterSponge();
		GameRegistry.registerWorldGenerator(worldGenSponge);
	}
}
