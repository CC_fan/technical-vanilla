package ccfan.technicalVanilla.worldGens;

import java.util.Random;

import ccfan.technicalVanilla.blocks.Blocks;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.BiomeGenOcean;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneraterSponge implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		
		if (random.nextInt(5)==0) return;
		
		int centerX = chunkX * 16 + random.nextInt(16);
		int centerZ = chunkZ * 16 + random.nextInt(16);

		if (world.getBiomeGenForCoords(centerX, centerZ) == BiomeGenBase.ocean){
			int centerY = getTopSolidBlock(world,centerX, centerZ) - 1;

			world.setBlock(centerX, centerY, centerZ, Blocks.blockSponge.blockID);
			world.setBlock(centerX+1, centerY, centerZ, Blocks.blockSponge.blockID);
			world.setBlock(centerX-1, centerY, centerZ, Blocks.blockSponge.blockID);
			world.setBlock(centerX, centerY+1, centerZ, Blocks.blockSponge.blockID);
			world.setBlock(centerX, centerY-1, centerZ, Blocks.blockSponge.blockID);
			world.setBlock(centerX, centerY, centerZ+1, Blocks.blockSponge.blockID);
			world.setBlock(centerX, centerY, centerZ-1, Blocks.blockSponge.blockID);
		}


	}
	
    /**
     * Finds the highest block on the x, z coordinate that is solid and returns its y coord. Args x, z
     */
    private int getTopSolidBlock(World world, int par1, int par2)
    {
        Chunk chunk = world.getChunkFromBlockCoords(par1, par2);
        int x = par1;
        int z = par2;
        int k = chunk.getTopFilledSegment() + 15;
        par1 &= 15;

        for (par2 &= 15; k > 0; --k)
        {
            int l = chunk.getBlockID(par1, k, par2);

            if (l != 0 && Block.blocksList[l].blockMaterial.blocksMovement() && Block.blocksList[l].blockMaterial != Material.leaves 
            		&& Block.blocksList[l].blockMaterial != Material.water && !Block.blocksList[l].isBlockFoliage(world, x, k, z))
            {
                return k + 1;
            }
        }

        return -1;
    }

}
