package ccfan.technicalVanilla.items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockRedstoneLogic;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import ccfan.technicalVanilla.ModInformation;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Items {
	public static Item itemLilyPad;
	
	public static void init(){
		itemLilyPad = new ItemLilyPad(ModInformation.BLOCK_POISON_LILY_PAD_ID - 256);
		GameRegistry.registerItem(itemLilyPad, ModInformation.ITEM_POISON_LILY_PAD_KEY);
	}
	
	public static void addNames() {
		LanguageRegistry.addName(itemLilyPad, ModInformation.BLOCK_POISON_LILY_PAD_NAME);
	}

}
