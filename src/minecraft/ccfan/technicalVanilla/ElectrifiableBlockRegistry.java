package ccfan.technicalVanilla;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.WorldProviderEnd;
import net.minecraft.world.WorldProviderHell;
import ccfan.technicalVanilla.blocks.IBlockElectrifiable;

import com.google.common.collect.Lists;

import cpw.mods.fml.common.ICraftingHandler;


public class ElectrifiableBlockRegistry {
	private static List<IBlockElectrifiable> ElectrifiableBlocks  = Lists.newArrayList();
		
	public static void RegisterElectrifiableBlock(IBlockElectrifiable block){
		ElectrifiableBlocks.add(block);
	}
	
	public enum BlockState{
		UNELECTRIFIABLE,
		ELECTRIFIABLE,
		ELECTRIFIZED}
		
	public static BlockState getBlockState(World world, int x, int y, int z){
		int id = world.getBlockId(x, y, z);
		for (IBlockElectrifiable block : ElectrifiableBlocks){
			if (id==block.getVanillaID()){
				if (block.canElectrifised(world, x, y, z)) return BlockState.ELECTRIFIABLE;
					else return BlockState.UNELECTRIFIABLE; }
			if (id==block.getElectrifiableID()) return BlockState.ELECTRIFIZED;
		}
		return BlockState.UNELECTRIFIABLE;
	}

	public static IBlockElectrifiable getBlock(World world, int x, int y, int z){
		int id = world.getBlockId(x, y, z);
		for (IBlockElectrifiable block : ElectrifiableBlocks){
			if ((id==block.getVanillaID() && block.canElectrifised(world, x, y, z)) || (id==block.getElectrifiableID())) return block;
		}
		return null;
	}

}
