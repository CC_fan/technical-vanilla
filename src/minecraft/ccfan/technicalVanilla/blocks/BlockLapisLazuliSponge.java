package ccfan.technicalVanilla.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import ccfan.technicalVanilla.ModInformation;
import ccfan.technicalVanilla.tileEntitys.TileEntityLapisLazuliSponge;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockLapisLazuliSponge extends BlockContainer {
	public BlockLapisLazuliSponge(int id) {
		super(id, Material.cloth);
		
		setCreativeTab(CreativeTabs.tabBlock);
		setHardness(0.6F);
		setStepSound(Block.soundGrassFootstep);
		setUnlocalizedName(ModInformation.BLOCK_LAPIS_SPONGE_UNLOCALIZED_NAME);
		func_111022_d(ModInformation.TEXTURE_LOCATION + ":" + ModInformation.BLOCK_LAPIS_SPONGE_ICON);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityLapisLazuliSponge();
	}
	
	@Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int par6, float par7, float par8, float par9){
		if (!world.isRemote){
			entityPlayer.sendChatToPlayer(new ChatMessageComponent().func_111079_a("Current Energy Level: " + ((TileEntityLapisLazuliSponge) world.getBlockTileEntity(x, y, z)).getEnergy() + "/60000"));
		}
		return true;
	}
	
	@Override
    public void harvestBlock(World world, EntityPlayer player, int x, int y, int z, int meta)
    {
        player.addStat(StatList.mineBlockStatArray[this.blockID], 1);
        player.addExhaustion(0.025F);
        Random random = new Random();
        
        float f = random.nextFloat() * 0.8F + 0.1F;
        float f1 = random.nextFloat() * 0.8F + 0.1F;
        float f2 = random.nextFloat() * 0.8F + 0.1F;
        
        EntityItem entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(Item.dyePowder.itemID , 1, 4));
        float f3 = 0.05F;
        entityitem.motionX = (double)((float)random.nextGaussian() * f3);
        entityitem.motionY = (double)((float)random.nextGaussian() * f3 + 0.2F);
        entityitem.motionZ = (double)((float)random.nextGaussian() * f3);

        world.spawnEntityInWorld(entityitem);
        
        if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().itemID == Item.shears.itemID && EnchantmentHelper.getSilkTouchModifier(player))
        {
            ItemStack itemstack = new ItemStack(Blocks.blockSponge.blockID, 1, 0);

            if (itemstack != null)
            {
                this.dropBlockAsItem_do(world, x, y, z, itemstack);
            }
        }
        else
        {
            int i1 = EnchantmentHelper.getFortuneModifier(player);
            this.dropBlockAsItem(world, x, y, z, meta, i1);
        }
    }
	
	    public int quantityDropped(Random par1Random)
    {
        return par1Random.nextInt(3) + 1;
    }
    
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return Item.stick.itemID;
    }
	
}
