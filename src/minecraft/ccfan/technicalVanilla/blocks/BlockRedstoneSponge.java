package ccfan.technicalVanilla.blocks;

import java.util.Random;

import com.google.common.collect.SetMultimap;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.stats.StatList;
import net.minecraft.world.World;
import ccfan.technicalVanilla.ModInformation;
import ccfan.technicalVanilla.tileEntitys.TileEntityLapisLazuliSponge;

public class BlockRedstoneSponge extends Block {
	public BlockRedstoneSponge(int id) {
		super(id, Material.cloth);
		
		setCreativeTab(CreativeTabs.tabBlock);
		setHardness(0.6F);
		setStepSound(Block.soundGrassFootstep);
		setUnlocalizedName(ModInformation.BLOCK_REDSTONE_SPONGE_UNLOCALIZED_NAME);
		func_111022_d(ModInformation.TEXTURE_LOCATION + ":" + ModInformation.BLOCK_REDSTONE_SPONGE_ICON);
	}
	
    public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta){
		int posdata = ( z + ((y + (x % 2)) % 2)) % 2; 		
		world.setBlockMetadataWithNotify(x, y, z, posdata, 2 + 4);
		return meta;			
    }
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		if (!world.isRemote){
			int meta = world.getBlockMetadata(x, y, z);
			int posdata = ( z+ ((y + (x % 2)) % 2)) % 2; 		
			world.setBlockMetadataWithNotify(x, y, z, posdata, 2 + 4);
			
			if (meta != posdata) {
				int x2, y2, z2;
				for ( int i = 1; true;) {
					x2=x+1 ; y2=y ; z2=z;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					x2=x ; y2=y ; z2=z+1;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					x2=x-1 ; y2=y ; z2=z;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					x2=x ; y2=y ; z2=z-1;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					x2=x ; y2=y+1 ; z2=z;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					x2=x ; y2=y-1 ; z2=z;
					if (getIsBlockLapisSponge(world, x2, y2, z2)){ break ;}
					return;
				}
	
				((TileEntityLapisLazuliSponge) world.getBlockTileEntity(x2, y2, z2)).addEnergy(500);
			}
		}	
	}
	
	@Override
    public void harvestBlock(World world, EntityPlayer player, int x, int y, int z, int meta)
    {
        player.addStat(StatList.mineBlockStatArray[this.blockID], 1);
        player.addExhaustion(0.025F);
        Random random = new Random();
        
        float f = random.nextFloat() * 0.8F + 0.1F;
        float f1 = random.nextFloat() * 0.8F + 0.1F;
        float f2 = random.nextFloat() * 0.8F + 0.1F;
        
        EntityItem entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(Item.redstone.itemID , 1, 0));
        float f3 = 0.05F;
        entityitem.motionX = (double)((float)random.nextGaussian() * f3);
        entityitem.motionY = (double)((float)random.nextGaussian() * f3 + 0.2F);
        entityitem.motionZ = (double)((float)random.nextGaussian() * f3);

        world.spawnEntityInWorld(entityitem);
        
        if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().itemID == Item.shears.itemID && EnchantmentHelper.getSilkTouchModifier(player))
        {
            ItemStack itemstack = new ItemStack(Blocks.blockSponge.blockID, 1, 0);

            if (itemstack != null)
            {
                this.dropBlockAsItem_do(world, x, y, z, itemstack);
            }
        }
        else
        {
            int i1 = EnchantmentHelper.getFortuneModifier(player);
            this.dropBlockAsItem(world, x, y, z, meta, i1);
        }
    }
	
	    public int quantityDropped(Random par1Random)
    {
        return par1Random.nextInt(3) + 1;
    }
    
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return Item.stick.itemID;
    }
	
	private boolean getIsBlockLapisSponge(World world, int x, int y, int z){
		return world.getBlockId(x,y,z) == ModInformation.BLOCK_LAPIS_SPONGE_ID;
	}


}
