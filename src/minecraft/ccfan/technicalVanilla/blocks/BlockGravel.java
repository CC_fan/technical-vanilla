package ccfan.technicalVanilla.blocks;

import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityFallingSand;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class BlockGravel extends Block implements IBlockElectrifiable
{
    /** Do blocks fall instantly to where they stop or do they fall over time */
    public static boolean fallInstantly;

    public BlockGravel(int par1)
    {
        super(par1, Material.sand);
        setHardness(0.6F);
        setStepSound(soundGravelFootstep);
        setUnlocalizedName(ModInformation.BLOCK_GRAVEL_UNLOCALIZED_NAME);
        func_111022_d("gravel");
        ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }

    public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
    	if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int idDropped(int par1, Random par2Random, int par3)
    {
        if (par3 > 3)
        {
            par3 = 3;
        }

        return par2Random.nextInt(10 - par3 * 3) == 0 ? Item.flint.itemID : getVanillaID();
    }
    
	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.gravel.blockID;
	}

	@Override
	public int getEnergyCost() {
		return 1;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
		
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
		
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}
}
