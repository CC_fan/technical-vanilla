package ccfan.technicalVanilla.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.IPlantable;

public class BlockCactus extends net.minecraft.block.BlockCactus implements IBlockElectrifiable
{
    @SideOnly(Side.CLIENT)
    private Icon cactusTopIcon;
    @SideOnly(Side.CLIENT)
    private Icon cactusBottomIcon;

    protected BlockCactus(int par1)
    {
        super(par1);
        this.setTickRandomly(true);
        setHardness(0.4F);
        setStepSound(soundClothFootstep);
        setUnlocalizedName(ModInformation.BLOCK_CACTUS_UNLOCALIZED_NAME);
        setCreativeTab((CreativeTabs)null );
        func_111022_d("cactus");
        ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }

    @Override
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
    	//doNothing!
    }
    @Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
    	if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_SAND_ID) 		 ok = true;			
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
    }
    
	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.cactus.blockID;
	}

	@Override
	public int getEnergyCost() {
		return 1;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}
}
