package ccfan.technicalVanilla.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCake extends net.minecraft.block.BlockCake implements IBlockElectrifiable
{
    @SideOnly(Side.CLIENT)
    private Icon cakeTopIcon;
    @SideOnly(Side.CLIENT)
    private Icon cakeBottomIcon;
    @SideOnly(Side.CLIENT)
    private Icon field_94382_c;

    protected BlockCake(int par1)
    {
        super(par1);
        this.setTickRandomly(true);
        setHardness(0.5F);
        setStepSound(soundClothFootstep);
        setUnlocalizedName(ModInformation.BLOCK_BEDROCK_UNLOCALIZED_NAME);
        disableStats();
        func_111022_d("cake");
        
        ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }


    @Override
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        this.eatCakeSlice(par1World, par2, par3, par4, par5EntityPlayer);
        return true;
    }

    /**
     * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
     */
    @Override
    public void onBlockClicked(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer)
    {
        this.eatCakeSlice(par1World, par2, par3, par4, par5EntityPlayer);
    }

    /**
     * Heals the player and removes a slice from the cake.
     */
    private void eatCakeSlice(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer)
    {
        if (!par1World.isRemote) par5EntityPlayer.sendChatToPlayer(new ChatMessageComponent().func_111079_a("The Cake is a lie!"));
    }
    
    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
        if (!this.canBlockStay(world, x, y, z))
        {
            world.setBlockToAir(x, y, z);
        }
        
        if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
    }

    
	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		// TODO Auto-generated method stub
		return Block.cake.blockID;
	}

	@Override
	public int getEnergyCost() {
		// TODO Auto-generated method stub
		return 1;
	}


	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}


	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}


	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}
}
