package ccfan.technicalVanilla.blocks;

import net.minecraft.world.World;

public interface IBlockElectrifiable {
	public int getElectrifiableID();
	public int getVanillaID();
	public int getEnergyCost();
	public void onElectrifising(World world, int x, int y, int z);
	public void onUnelectrifising(World world, int x, int y, int z);
	public boolean canElectrifised(World world, int x, int y, int z);
	public boolean shouldBlockUpdate(World world, int x, int y, int z);
}
