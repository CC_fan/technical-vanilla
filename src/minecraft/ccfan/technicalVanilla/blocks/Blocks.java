package ccfan.technicalVanilla.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockRedstoneLogic;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import ccfan.technicalVanilla.ModInformation;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Blocks {
	public static Block blockSponge;
	public static Block blockRedstoneSponge;
	public static BlockContainer blockLapisSponge;
	public static Block blockBedrock;
	public static Block blockCake;
	public static Block blockCactus;
	public static Block blockSand;
	public static Block blockGravel;
	public static Block blockNetherrack;
	public static Block blockRedstoneTorchActive;
	public static Block blockRedstoneTorchInactive;
	public static BlockRedstoneLogic blockRedstoneRepeaterActive;
	public static BlockRedstoneLogic blockRedstoneRepeaterInactive;
	public static BlockPistonBase blockPistonBase;
	public static BlockPistonBase blockPistonStickyBase;
	public static BlockPistonExtension blockPistonExtension;
	public static Block blockAnvil;
	public static Block blockBrewingStand;
	public static Block blockLilyPad;
	public static Block blockPoisonLilyPad;
	public static BlockContainer blockDragonEgg;
	public static BlockHopper blockHopper;
	public static BlockContainer blockTrappedChest;
	public static Block blockIronBars;
	
	public static void init(){
		blockSponge = new BlockSponge(ModInformation.BLOCK_SPONGE_ID);
		GameRegistry.registerBlock(blockSponge, ModInformation.BLOCK_SPONGE_KEY);
		OreDictionary.registerOre("blockSponge", new ItemStack(blockSponge));

		blockRedstoneSponge = new BlockRedstoneSponge(ModInformation.BLOCK_REDSTONE_SPONGE_ID);
		GameRegistry.registerBlock(blockRedstoneSponge, ModInformation.BLOCK_REDSTONE_SPONGE_KEY);

		blockLapisSponge = new BlockLapisLazuliSponge(ModInformation.BLOCK_LAPIS_SPONGE_ID);
		GameRegistry.registerBlock(blockLapisSponge, ModInformation.BLOCK_LAPIS_SPONGE_KEY);

		blockPoisonLilyPad = new BlockPoisonLilyPad(ModInformation.BLOCK_POISON_LILY_PAD_ID);
		GameRegistry.registerBlock(blockPoisonLilyPad, ModInformation.BLOCK_POISON_LILY_PAD_KEY);

		
		
		blockBedrock = new BlockBedrock(ModInformation.BLOCK_BEDROCK_ID);
		GameRegistry.registerBlock(blockBedrock, ModInformation.BLOCK_BEDROCK_KEY);

		blockCake = new BlockCake(ModInformation.BLOCK_CAKE_ID);
		GameRegistry.registerBlock(blockCake, ModInformation.BLOCK_CAKE_KEY);

		blockCactus = new BlockCactus(ModInformation.BLOCK_CACTUS_ID);
		GameRegistry.registerBlock(blockCactus, ModInformation.BLOCK_CACTUS_KEY);

		blockSand = new BlockSand(ModInformation.BLOCK_SAND_ID);
		GameRegistry.registerBlock(blockSand, ModInformation.BLOCK_SAND_KEY);

		blockGravel = new BlockGravel(ModInformation.BLOCK_GRAVEL_ID);
		GameRegistry.registerBlock(blockGravel, ModInformation.BLOCK_GRAVEL_KEY);

		blockNetherrack = new BlockNetherrack(ModInformation.BLOCK_NETHERRACK_ID);
		GameRegistry.registerBlock(blockNetherrack, ModInformation.BLOCK_NETHERRACK_KEY);

		blockRedstoneTorchActive = new BlockRedstoneTorch(ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_ID, true);
		GameRegistry.registerBlock(blockRedstoneTorchActive, ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_KEY);

		blockRedstoneTorchInactive = new BlockRedstoneTorch(ModInformation.BLOCK_REDSTONE_TORCH_IDLE_ID, false);
		GameRegistry.registerBlock(blockRedstoneTorchInactive, ModInformation.BLOCK_REDSTONE_TORCH_IDLE_KEY);

		blockRedstoneRepeaterActive = new BlockRedstoneRepeater(ModInformation.BLOCK_REDSTONE_REPEATER_ACTIVE_ID, true);
		GameRegistry.registerBlock(blockRedstoneRepeaterActive, ModInformation.BLOCK_REDSTONE_REPEATER_ACTIVE_KEY);

		blockRedstoneRepeaterInactive = new BlockRedstoneRepeater(ModInformation.BLOCK_REDSTONE_REPEATER_IDLE_ID, false);
		GameRegistry.registerBlock(blockRedstoneRepeaterInactive, ModInformation.BLOCK_REDSTONE_REPEATER_IDLE_KEY);

		blockPistonBase = new BlockPistonBase(ModInformation.BLOCK_PISTON_BASE_ID, false);
		GameRegistry.registerBlock(blockPistonBase, ModInformation.BLOCK_PISTON_BASE_KEY);

		blockPistonStickyBase = new BlockPistonBase(ModInformation.BLOCK_PISTON_STICKY_BASE_ID, true);
		GameRegistry.registerBlock(blockPistonStickyBase, ModInformation.BLOCK_PISTON_STICKY_BASE_KEY);

		blockPistonExtension = new BlockPistonExtension(ModInformation.BLOCK_PISTON_EXTENSION_ID);
		GameRegistry.registerBlock(blockRedstoneRepeaterInactive, ModInformation.BLOCK_PISTON_EXTENSION_KEY);

		blockAnvil = new BlockAnvil(ModInformation.BLOCK_ANVIL_ID);
		GameRegistry.registerBlock(blockAnvil, ModInformation.BLOCK_ANVIL_KEY);

		blockBrewingStand = new BlockBrewingStand(ModInformation.BLOCK_BREWING_STAND_ID);
		GameRegistry.registerBlock(blockBrewingStand, ModInformation.BLOCK_BREWING_STAND_KEY);

		blockLilyPad = new BlockLilyPad(ModInformation.BLOCK_LILY_PAD_ID);
		GameRegistry.registerBlock(blockLilyPad, ModInformation.BLOCK_LILY_PAD_KEY);

		blockDragonEgg = new BlockDragonEgg(ModInformation.BLOCK_DRAGON_EGG_ID);
		GameRegistry.registerBlock(blockDragonEgg, ModInformation.BLOCK_DRAGON_EGG_KEY);

		blockHopper = new BlockHopper(ModInformation.BLOCK_HOPPER_ID);
		GameRegistry.registerBlock(blockHopper, ModInformation.BLOCK_HOPPER_KEY);

		blockTrappedChest = new BlockTrappedChest(ModInformation.BLOCK_TRAPPED_CHEST_ID);
		GameRegistry.registerBlock(blockHopper, ModInformation.BLOCK_TRAPPED_CHEST_KEY);

		blockIronBars = new BlockIronBars(ModInformation.BLOCK_IRON_BARS_ID);
		GameRegistry.registerBlock(blockIronBars, ModInformation.BLOCK_IRON_BARS_KEY);
}
	
	public static void addNames() {
		LanguageRegistry.addName(blockSponge, ModInformation.BLOCK_SPONGE_NAME);
		LanguageRegistry.addName(blockRedstoneSponge, ModInformation.BLOCK_REDSTONE_SPONGE_NAME);		
		LanguageRegistry.addName(blockLapisSponge, ModInformation.BLOCK_LAPIS_SPONGE_NAME);		
		LanguageRegistry.addName(blockPoisonLilyPad, ModInformation.BLOCK_POISON_LILY_PAD_NAME);

		
		LanguageRegistry.addName(blockBedrock, ModInformation.BLOCK_BEDROCK_NAME);
		LanguageRegistry.addName(blockCake, ModInformation.BLOCK_CAKE_NAME);
		LanguageRegistry.addName(blockCactus, ModInformation.BLOCK_CACTUS_NAME);
		LanguageRegistry.addName(blockSand, ModInformation.BLOCK_SAND_NAME);
		LanguageRegistry.addName(blockGravel, ModInformation.BLOCK_GRAVEL_NAME);
		LanguageRegistry.addName(blockNetherrack, ModInformation.BLOCK_NETHERRACK_NAME);
		LanguageRegistry.addName(blockRedstoneTorchActive, ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_NAME);
		LanguageRegistry.addName(blockRedstoneTorchInactive, ModInformation.BLOCK_REDSTONE_TORCH_IDLE_NAME);
		LanguageRegistry.addName(blockPistonBase, ModInformation.BLOCK_PISTON_BASE_NAME);
		LanguageRegistry.addName(blockPistonStickyBase, ModInformation.BLOCK_PISTON_STICKY_BASE_NAME);
		LanguageRegistry.addName(blockAnvil, ModInformation.BLOCK_ANVIL_NAME);
		LanguageRegistry.addName(blockBrewingStand, ModInformation.BLOCK_BREWING_STAND_NAME);
		LanguageRegistry.addName(blockLilyPad, ModInformation.BLOCK_LILY_PAD_NAME);
		LanguageRegistry.addName(blockDragonEgg, ModInformation.BLOCK_DRAGON_EGG_NAME);
		LanguageRegistry.addName(blockHopper, ModInformation.BLOCK_HOPPER_NAME);
		LanguageRegistry.addName(blockTrappedChest, ModInformation.BLOCK_TRAPPED_CHEST_NAME);
		LanguageRegistry.addName(blockIronBars, ModInformation.BLOCK_IRON_BARS_NAME);
	}

}
