package ccfan.technicalVanilla.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockSand;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.item.EntityFallingSand;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import ccfan.technicalVanilla.proxies.ClientProxy;
import ccfan.technicalVanilla.tileEntitys.TileEntityDragonEgg;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockDragonEgg extends BlockContainer implements IBlockElectrifiable
{
    public BlockDragonEgg(int par1)
    {
        super(par1, Material.dragonEgg);
        this.setBlockBounds(0.0625F, 0.0F, 0.0625F, 0.9375F, 1.0F, 0.9375F);
        setHardness(3.0F);
        setResistance(15.0F);
        setStepSound(soundStoneFootstep);
        setLightValue(0.125F);
        setUnlocalizedName(ModInformation.BLOCK_DRAGON_EGG_UNLOCALIZED_NAME);
        func_111022_d("dragon_egg");
        ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }

    
    
    
    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        par1World.scheduleBlockUpdate(par2, par3, par4, this.blockID, this.tickRate(par1World));
    }

    /**
     * Lets the block know when one of its neighbor changes. Doesn't know which neighbor changed (coordinates passed are
     * their own) Args: x, y, z, neighbor blockID
     */
    public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
        world.scheduleBlockUpdate(x, y, z, this.blockID, this.tickRate(world));
		if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        this.fallIfPossible(par1World, par2, par3, par4);
    }

    /**
     * Checks if the dragon egg can fall down, and if so, makes it fall.
     */
    private void fallIfPossible(World par1World, int par2, int par3, int par4)
    {
        if (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4) && par3 >= 0)
        {
            byte b0 = 32;

            if (!BlockSand.fallInstantly && par1World.checkChunksExist(par2 - b0, par3 - b0, par4 - b0, par2 + b0, par3 + b0, par4 + b0))
            {
                EntityFallingSand entityfallingsand = new EntityFallingSand(par1World, (double)((float)par2 + 0.5F), (double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), this.blockID);
                par1World.spawnEntityInWorld(entityfallingsand);
            }
            else
            {
                par1World.setBlockToAir(par2, par3, par4);

                while (BlockSand.canFallBelow(par1World, par2, par3 - 1, par4) && par3 > 0)
                {
                    --par3;
                }

                if (par3 > 0)
                {
                    par1World.setBlock(par2, par3, par4, this.blockID, 0, 2);
                }
            }
        }
    }
    
	@Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }
    
    /**
     * How many world ticks before ticking
     */
    public int tickRate(World par1World)
    {
        return 5;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube()
    {
        return false;
    }

    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @SideOnly(Side.CLIENT)

    /**
     * Returns true if the given side of this block type should be rendered, if the adjacent block is at the given
     * coordinates.  Args: blockAccess, x, y, z, side
     */
    public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
    {
        return true;
    }

    /**
     * The type of render function that is called for this block
     */
    public int getRenderType()
    {
        return ClientProxy.DragonEggRenderType;
    }
    

    @SideOnly(Side.CLIENT)

    /**
     * only called by clickMiddleMouseButton , and passed to inventory.setCurrentItem (along with isCreative)
     */
    public int idPicked(World par1World, int par2, int par3, int par4)
    {
        return 0;
    }

	@Override
	public int getElectrifiableID() {
		return this.blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.dragonEgg.blockID;
	}

	@Override
	public int getEnergyCost() {
		return 50;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityDragonEgg();
	}




	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}
}
