package ccfan.technicalVanilla.blocks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;

public class BlockRedstoneTorch extends net.minecraft.block.BlockRedstoneTorch implements IBlockElectrifiable{
    private boolean torchActive;

	protected BlockRedstoneTorch(int par1, boolean par2) {
		super(par1, par2);
		setHardness(0.0F);
		setStepSound(soundWoodFootstep);
		setUnlocalizedName(ModInformation.BLOCK_REDSTONE_TORCH_ACTIVE_UNLOCALIZED_NAME);
		if (!par2){
			func_111022_d("redstone_torch_off");
		}else{
			setLightValue(0.5F);
			func_111022_d("redstone_torch_on");
		}
		torchActive = par2;
		
		ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
	}
	
    /** Map of ArrayLists of RedstoneUpdateInfo. Key of map is World. */
    private static Map redstoneUpdateInfoCache = new HashMap();
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
		super.onNeighborBlockChange(world, x, y, z, id);
	}
    
    /**
     * Returns true or false based on whether the block the torch is attached to is providing indirect power.
     */
    private boolean isIndirectlyPowered(World par1World, int par2, int par3, int par4)
    {
        int l = par1World.getBlockMetadata(par2, par3, par4);
        return l == 5 && par1World.getIndirectPowerOutput(par2, par3 - 1, par4, 0) ? true : (l == 3 && par1World.getIndirectPowerOutput(par2, par3, par4 - 1, 2) ? true : (l == 4 && par1World.getIndirectPowerOutput(par2, par3, par4 + 1, 3) ? true : (l == 1 && par1World.getIndirectPowerOutput(par2 - 1, par3, par4, 4) ? true : l == 2 && par1World.getIndirectPowerOutput(par2 + 1, par3, par4, 5))));
    }
    
	
	/**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        boolean flag = this.isIndirectlyPowered(par1World, par2, par3, par4);

        
        if (this.torchActive)
        {
            if (flag)
            {
                par1World.setBlock(par2, par3, par4, Block.torchRedstoneIdle.blockID, par1World.getBlockMetadata(par2, par3, par4), 3);

            }
        }
        else if (!flag)
        {
            par1World.setBlock(par2, par3, par4, Block.torchRedstoneActive.blockID, par1World.getBlockMetadata(par2, par3, par4), 3);
        }
    }

	
	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return (this.torchActive ? Block.torchRedstoneActive.blockID : Block.torchRedstoneIdle.blockID);
	}

	@Override
	public int getEnergyCost() {
		return 2;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return true;
	}

	
}
