package ccfan.technicalVanilla.blocks;

import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityFallingSand;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.MinecraftForge;

public class BlockSand extends Block implements IBlockElectrifiable
{
    /** Do blocks fall instantly to where they stop or do they fall over time */
    public static boolean fallInstantly;

    public BlockSand(int par1)
    {
        super(par1, Material.sand);
        setHardness(0.5F);
        setStepSound(soundSandFootstep);
        setUnlocalizedName(ModInformation.BLOCK_SAND_UNLOCALIZED_NAME);
        func_111022_d("sand");
        ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int par5)
    {
    	if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
    }

    @Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }

    
	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.sand.blockID;
	}

	@Override
	public int getEnergyCost() {
		return 1;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
		if (world.getBlockId(x, y+1, z) == Block.cactus.blockID){
			world.setBlock(x, y+1, z, Blocks.blockCactus.blockID, 0, 2);
			if (world.getBlockId(x, y+2, z) == Block.cactus.blockID){
				world.setBlock(x, y+2, z, Blocks.blockCactus.blockID, 0, 2);
			}
		}
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
		if (world.getBlockId(x, y+1, z) == Blocks.blockCactus.blockID){
			world.setBlock(x, y+1, z, Block.cactus.blockID, 0, 2);
			if (world.getBlockId(x, y+2, z) == Blocks.blockCactus.blockID){
				world.setBlock(x, y+2, z, Block.cactus.blockID, 0, 2);
			}
		}
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return true;
	}
}
