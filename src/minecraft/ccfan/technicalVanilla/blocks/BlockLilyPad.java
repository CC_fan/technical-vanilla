package ccfan.technicalVanilla.blocks;

import java.util.List;
import java.util.Random;

import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockLilyPad extends net.minecraft.block.BlockLilyPad implements IBlockElectrifiable
{
    protected BlockLilyPad(int par1)
    {
        super(par1);
        float f = 0.5F;
        float f1 = 0.015625F;
        this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f1, 0.5F + f);
        this.setCreativeTab((CreativeTabs)null);
        setHardness(0.0F);
        setStepSound(soundGrassFootstep);
        setUnlocalizedName(ModInformation.BLOCK_LILY_PAD_UNLOCALIZED_NAME);
        func_111022_d("waterlily");
		setTickRandomly(true);
		ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
    }

    
    public void updateTick(World world, int x, int y, int z, Random rand)
    {
		super.updateTick(world, x, y, z, rand);
    	if (rand.nextInt(5)==0){
			int meta = world.getBlockMetadata(x, y, z);
			if (meta == 15){
				world.setBlock(x, y, z, Blocks.blockPoisonLilyPad.blockID, 0, 3);
			} else {
				world.setBlockMetadataWithNotify(x, y, z, meta+1, 2);
			}
		}
	}
    
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		super.onNeighborBlockChange(world, x, y, z, id);
		if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
	}
	
	@Override
    public int idPicked(World par1World, int par2, int par3, int par4)
    {
        return this.getVanillaID();
    }
    
	
	@Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }
    

	@Override
	public int getElectrifiableID() {
		return this.blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.waterlily.blockID;
	}

	@Override
	public int getEnergyCost() {
		return 5;
	}

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
		world.setBlockMetadataWithNotify(x, y, z, 0, 2);
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
		world.setBlockMetadataWithNotify(x, y, z, 0, 3);
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return true;
	}


	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}
}
