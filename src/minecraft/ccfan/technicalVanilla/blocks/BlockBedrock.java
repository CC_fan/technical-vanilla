package ccfan.technicalVanilla.blocks;

import java.util.Random;

import com.google.common.collect.SetMultimap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldProviderEnd;
import net.minecraft.world.WorldProviderHell;
import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;
import ccfan.technicalVanilla.ElectrifiableBlockRegistry.BlockState;

public class BlockBedrock extends Block implements IBlockElectrifiable{
	public BlockBedrock(int id) {
		super(id, Material.rock);
		setResistance(6000000.0F);
		setHardness(10F);
		setStepSound(soundStoneFootstep);
		setUnlocalizedName(ModInformation.BLOCK_BEDROCK_UNLOCALIZED_NAME);
		func_111022_d("bedrock");
		ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
		
	}
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
	}
	

	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.bedrock.blockID;
	}



	@Override
	public int getEnergyCost() {
		return 15;
	}
	
	
	@Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		if (((y == 0) &&(!(world.provider instanceof WorldProviderEnd))) || (world.provider instanceof WorldProviderHell && y==127)) return false;
			else return true;
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}

}
