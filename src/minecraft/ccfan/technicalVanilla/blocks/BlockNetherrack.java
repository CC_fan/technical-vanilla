package ccfan.technicalVanilla.blocks;

import java.util.Random;

import cpw.mods.fml.common.Mod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraft.world.WorldProviderHell;
import ccfan.technicalVanilla.ElectrifiableBlockRegistry;
import ccfan.technicalVanilla.ModInformation;

public class BlockNetherrack extends Block implements IBlockElectrifiable{
	public BlockNetherrack(int id) {
		super(id, Material.rock);

		setHardness(0.4F);
		setStepSound(soundStoneFootstep);
		setUnlocalizedName(ModInformation.BLOCK_NETHERRACK_UNLOCALIZED_NAME);
		func_111022_d("stone");
		setTickRandomly(true);
		
		ElectrifiableBlockRegistry.RegisterElectrifiableBlock(this);
		
	}
	
	public void updateTick(World world, int x, int y, int z, Random rand)
    {
		for (int i = 1; i<400; i++){
			int TargetX = Math.round(rand.nextFloat()*32 - 16);
			int TargetY = Math.round(rand.nextFloat()*32 - 16);
			int TargetZ = Math.round(rand.nextFloat()*32 - 16);
	    
			if (TargetX == 0 && TargetY == 0 && TargetZ == 0) return;
			System.out.println(TargetX + ", " + TargetY + ", " + TargetZ);
			//if (Math.sqrt(TargetX^2 + TargetY^2 + TargetZ^2) <16){
				System.out.println(TargetX + ", " + TargetY + ", " + TargetZ);
				ChangeBlock(world, x + TargetX, y + TargetY, z + TargetZ, rand);
			//}
		}
	}
	
	
	
	public void ChangeBlock(World world, int x, int y, int z, Random rand){
		int id = world.getBlockId(x, y, z);
		int meta = world.getBlockMetadata(x, y, z);
		int targetID = id;
		int targetMeta = meta;
		if (id==Block.netherrack.blockID){ 
			if (world.getBlockId(x, y+1,z) == 0){
				targetID = Block.grass.blockID;
			} else if ((world.getBlockId(x, y+2,z) == 0) || (world.getBlockId(x, y+3,z) == 0)) {
				targetID = Block.dirt.blockID;
			} else {
				targetID = Block.stone.blockID;
			}
		} else if (id==Block.slowSand.blockID){
			targetID = Block.sand.blockID;
		} else if (id==Block.netherBrick.blockID){
			targetID = Block.stoneBrick.blockID;
			targetMeta = (int) (rand.nextFloat()*3);
		} else if (id==Block.stairsNetherBrick.blockID){
			targetID = Block.stairsStoneBrick.blockID;
		} else if (id==Block.netherFence.blockID){
			targetID = Block.fence.blockID;
		} else if (id==Block.lavaStill.blockID){
			targetID = Block.waterStill.blockID;
		} else if (id==Block.lavaMoving.blockID){
			targetID = Block.waterMoving.blockID;
		}
		
		world.setBlock(x, y, z, targetID, targetMeta, 2);
	}
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		if (!world.isRemote){
			boolean ok = false;
			if (world.getBlockId(x-1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x+1, y, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y-1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y+1, z) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z-1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (world.getBlockId(x, y, z+1) == ModInformation.BLOCK_LAPIS_SPONGE_ID) ok = true;
			if (!ok){
				world.setBlock(x, y, z, getVanillaID(), world.getBlockMetadata(x, y, z), 3);
				onUnelectrifising(world, x, y, z);
			}
		}
	}
	

	@Override
	public int getElectrifiableID() {
		return blockID;
	}

	@Override
	public int getVanillaID() {
		return Block.netherrack.blockID;
	}



	@Override
	public int getEnergyCost() {
		return 30;
	}
	
	
	@Override
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.getVanillaID();
    }

	@Override
	public void onElectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean canElectrifised(World world, int x, int y, int z) {
		return (world.provider instanceof WorldProviderHell);
	}

	@Override
	public void onUnelectrifising(World world, int x, int y, int z) {
	}

	@Override
	public boolean shouldBlockUpdate(World world, int x, int y, int z) {
		return false;
	}

}
