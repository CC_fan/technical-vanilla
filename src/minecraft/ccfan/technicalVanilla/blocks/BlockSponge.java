package ccfan.technicalVanilla.blocks;

import java.util.Random;

import ccfan.technicalVanilla.ModInformation;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.MinecraftForge;

public class BlockSponge extends Block {
	public BlockSponge(int id) {
		super(id, Material.cloth);
		
		setCreativeTab(CreativeTabs.tabBlock);
		setHardness(0.6F);
		setStepSound(Block.soundGrassFootstep);
		setUnlocalizedName(ModInformation.BLOCK_SPONGE_UNLOCALIZED_NAME);
		func_111022_d("sponge");
		MinecraftForge.setBlockHarvestLevel(this, "shears", 0);
	}
	
    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	if (player.getHeldItem() != null && player.getHeldItem().itemID == Item.redstone.itemID) {
    		world.setBlock(x, y, z, Blocks.blockRedstoneSponge.blockID, 0, 3);
    		player.getHeldItem().stackSize--;
    		return true;
    	}

    	if (player.getHeldItem() != null && player.getHeldItem().itemID == Item.dyePowder.itemID && player.getHeldItem().getItemDamage() == 4) {
    		world.setBlock(x, y, z, Blocks.blockLapisSponge.blockID, 0, 3);
    		player.getHeldItem().stackSize--;
    		return true;
    	}

    	
        return false;
    }
    
	
    /**
     * Called when the player destroys a block with an item that can harvest it. (i, j, k) are the coordinates of the
     * block and l is the block's subtype/damage.
     */
	@Override
    public void harvestBlock(World world, EntityPlayer player, int x, int y, int z, int meta)
    {
        player.addStat(StatList.mineBlockStatArray[this.blockID], 1);
        player.addExhaustion(0.025F);
        if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().itemID == Item.shears.itemID && EnchantmentHelper.getSilkTouchModifier(player))
        {
            ItemStack itemstack = this.createStackedBlock(meta);

            if (itemstack != null)
            {
                this.dropBlockAsItem_do(world, x, y, z, itemstack);
            }
        }
        else
        {
            int i1 = EnchantmentHelper.getFortuneModifier(player);
            this.dropBlockAsItem(world, x, y, z, meta, i1);
        }
    }
	
	
    public int quantityDropped(Random par1Random)
    {
        return par1Random.nextInt(3) + 1;
    }
    
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return Item.stick.itemID;
    }

}
