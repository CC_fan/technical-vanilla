package ccfan.technicalVanilla;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ccfan.technicalVanilla.blocks.Blocks;
import ccfan.technicalVanilla.containers.ContainerBrewingStand;
import ccfan.technicalVanilla.containers.ContainerRepair;
import ccfan.technicalVanilla.guis.GuiBrewingStand;
import ccfan.technicalVanilla.guis.GuiRepair;
import ccfan.technicalVanilla.tileEntitys.TileEntityBrewingStand;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

		int id = world.getBlockId(x, y, z);
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

		if (id == Blocks.blockAnvil.blockID){
			return new ContainerRepair(player.inventory, world, x, y, z, player);
		}
		
		if (id == Blocks.blockBrewingStand.blockID && tileEntity instanceof TileEntityBrewingStand){
			return new ContainerBrewingStand(player.inventory, (TileEntityBrewingStand) tileEntity);
		}
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		
		int id = world.getBlockId(x, y, z);
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		
		
		if (id == Blocks.blockAnvil.blockID){
			return new GuiRepair(player.inventory, world, x, y, z);
		}
		
		if (id == Blocks.blockBrewingStand.blockID && tileEntity instanceof TileEntityBrewingStand){
			return new GuiBrewingStand(player.inventory, (TileEntityBrewingStand) tileEntity);
		}
		
		return null;
	}
	

}
