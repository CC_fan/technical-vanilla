package ccfan.technicalVanilla;

import net.minecraft.src.ModLoader;
import net.minecraftforge.transformers.ForgeAccessTransformer;
import ccfan.technicalVanilla.blocks.Blocks;
import ccfan.technicalVanilla.entity.EntityDragon;
import ccfan.technicalVanilla.items.Items;
import ccfan.technicalVanilla.network.PacketHandler;
import ccfan.technicalVanilla.proxies.CommonProxy;
import ccfan.technicalVanilla.tileEntitys.TileEntityBrewingStand;
import ccfan.technicalVanilla.tileEntitys.TileEntityDragonEgg;
import ccfan.technicalVanilla.tileEntitys.TileEntityHopper;
import ccfan.technicalVanilla.tileEntitys.TileEntityLapisLazuliSponge;
import ccfan.technicalVanilla.worldGens.WorldGens;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION)
@NetworkMod(channels = {ModInformation.CHANNEL}, clientSideRequired = true, serverSideRequired = false, packetHandler = PacketHandler.class)
public class TechnicalVanilla {
	@Instance(ModInformation.ID)
	public static TechnicalVanilla instance;
	
	
	@SidedProxy(clientSide = "ccfan.technicalVanilla.proxies.ClientProxy", serverSide = "ccfan.technicalVanilla.proxies.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		ConfigHandler.init(event.getSuggestedConfigurationFile());
		Blocks.init();
		Items.init();
	}
	
	@EventHandler
	public void load(FMLInitializationEvent event) {
		Blocks.addNames();
		Items.addNames();
		GameRegistry.registerTileEntity(TileEntityLapisLazuliSponge.class, "LapisSpongeTV");
		GameRegistry.registerTileEntity(TileEntityBrewingStand.class, "BrewingStandTV");
		GameRegistry.registerTileEntity(TileEntityDragonEgg.class, "DragonEggTV");
		GameRegistry.registerTileEntity(TileEntityHopper.class, "HopperTV");
		
		EntityRegistry.registerGlobalEntityID(EntityDragon.class, ModInformation.ENTITY_ENDERDRAGON_KEY, EntityRegistry.findGlobalUniqueEntityId());
		EntityRegistry.registerModEntity(EntityDragon.class, ModInformation.ENTITY_ENDERDRAGON_KEY, 0, this, 128, 1, false);
		LanguageRegistry.instance().addStringLocalization("entity." + ModInformation.ENTITY_ENDERDRAGON_KEY + ".name", ModInformation.ENTITY_ENDERDRAGON_NAME);

		LanguageRegistry.instance().addStringLocalization("death.attack.Electric", ModInformation.DEATH_TRAPPEDCHEST);
		
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
		WorldGens.init();
		proxy.setCustomRenderers();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
	
	
}
